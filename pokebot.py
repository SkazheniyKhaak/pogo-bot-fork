# Credits to Tejado and the devs over at the subreddit /r/PokemonGoDev -TomTheBotter

# Do NOT use this bot while signed into your phone -TomTheBotter

# If you want to make the bot as fast as possible for catching more pokemon (like you are driving a fast car--it won't look suspicious though, since it only goes on paths), make stepsize 200; for more info see that method in pgoapi.py under pgoapi

import argparse
import json
import logging
import os
from time import sleep
from traceback import print_exc

from geopy.geocoders import GoogleV3

from worker import Worker

log = logging.getLogger(__name__)


def get_pos_by_name(location_name):
    geolocator = GoogleV3()
    loc = geolocator.geocode(location_name)
    return loc.latitude, loc.longitude, loc.altitude


def init_config():
    parser = argparse.ArgumentParser()
    config_file = "config.json"
    load = {}
    if os.path.isfile(config_file):
        with open(config_file) as data:
            load.update(json.load(data))

    required = lambda x: x not in load['accounts'][0].keys()
    parser.add_argument("-a", "--auth_service", help="Auth Service ('ptc' or 'google')",
                        required=required("auth_service"))
    parser.add_argument("-i", "--config_index", help="config_index")
    parser.add_argument("-u", "--username", help="Username", required=required("username"))
    parser.add_argument("-p", "--password", help="Password", required=required("password"))
    parser.add_argument("-l", "--location", help="Location", required=required("location"))
    parser.add_argument("-d", "--debug", help="Debug Mode", action='store_true')
    parser.add_argument("-c", "--cached", help="cached", action='store_true')
    parser.add_argument("-t", "--test", help="Only parse the specified location", action='store_true')
    parser.set_defaults(DEBUG=False, TEST=False, CACHED=False)
    config = parser.parse_args()
    load = load['accounts'][int(config.__dict__.get('config_index', 0) or 0)]
    config.__dict__.update(load)
    if config.auth_service not in ['ptc', 'google']:
        log.error("Invalid Auth service specified! ('ptc' or 'google')")
        return None

    return config


def main():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(module)10s] [%(levelname)5s] %(message)s')
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("pgoapi").setLevel(logging.INFO)
    logging.getLogger("rpc_api").setLevel(logging.INFO)

    config = init_config()
    if not config:
        return

    if config.debug:
        logging.getLogger("requests").setLevel(logging.INFO)
        logging.getLogger("pgoapi").setLevel(logging.INFO)
        logging.getLogger("rpc_api").setLevel(logging.INFO)

    config.location = get_pos_by_name(config.location)
    if 'walk_to' in config.__dict__:
        config.walk_to = get_pos_by_name(config.walk_to)
    if config.test:
        return

    pokemon_names = json.load(open("name_id.json"))
    api = Worker(config.__dict__, pokemon_names)

    while True:
        try:
            api.main_loop()
        except Exception as e:
            log.error('Main loop has an ERROR, restarting %s', e)
            print_exc()
            sleep(30)
            api = Worker(config.__dict__, pokemon_names)
            # main()
        except KeyboardInterrupt:
            return


if __name__ == '__main__':
    main()
