from __future__ import absolute_import

import json
import logging
import os
from OpenSSL.SSL import SysCallError
from collections import defaultdict
from itertools import chain, imap
from random import gauss, choice, random
from time import time, strftime, sleep
from traceback import print_exc

import pyproj
from geopy.distance import vincenty
from gmaps.directions import *
from gmaps.errors import NoResults, GmapException

import pgoapi.protos.POGOProtos.Enums.TeamColor_pb2 as Enums
import pgoapi.protos.POGOProtos.Inventory.Item.ItemId_pb2 as Inventory
from pgoapi.exceptions import ServerSideRequestThrottlingException, NotLoggedInException
from pgoapi.pgoapi import PGoApi
from pgoapi.utilities import f2i, get_cell_ids

PD = 'pokemon_data'
R = 'responses'
Lt = 'latitude'
Lg = 'longitude'
Un = 'username'
OBT = 'owned_by_team'
St = 'stamina'
Sc = 'stardust_cost'
Pid = 'pokemon_id'
Sm = 'stamina_max'
Ca = 'candy'
Fid = 'family_id'
Ea = 'experience_awarded'
N = 'name'
ID = 'id'
I = 'item'
C = 'count'
Ii = 'item_id'
Re = 'result'
T = 'team'
l = 'level'
DIM = 'distance_in_meters'

logger = logging.getLogger(__name__)

# Minimum amount of the bad items that you should have ... Modify based on your needs ... like if you need to battle a gym?
ITEM_COUNTS = {Inventory.ITEM_POKE_BALL: lambda l: 20 + l,
               Inventory.ITEM_GREAT_BALL: lambda l: 30 + l,
               Inventory.ITEM_ULTRA_BALL: lambda l: 40 + l,
               # Inventory.ITEM_MASTER_BALL: lambda l: 40 + l,

               Inventory.ITEM_POTION: lambda l: 0,  # 40 - 2 * l,
               Inventory.ITEM_SUPER_POTION: lambda l: 0,  # 40 - l,
               Inventory.ITEM_HYPER_POTION: lambda l: 100,  # 40 + l,
               Inventory.ITEM_MAX_POTION: lambda l: 100,  # 40 + l,

               Inventory.ITEM_RAZZ_BERRY: lambda l: 20,
               Inventory.ITEM_REVIVE: lambda l: 40,  # 20 if l < 20 else 10,
               Inventory.ITEM_MAX_REVIVE: lambda l: 10 + l}
ITEM_NAME = {Inventory.ITEM_POKE_BALL: 'Poke ball',
             Inventory.ITEM_GREAT_BALL: 'Great ball',
             Inventory.ITEM_ULTRA_BALL: 'Ultra ball',
             Inventory.ITEM_MASTER_BALL: 'Master ball',
             Inventory.ITEM_POTION: 'Potion',
             Inventory.ITEM_SUPER_POTION: 'Super potion',
             Inventory.ITEM_HYPER_POTION: 'Hyper potion',
             Inventory.ITEM_MAX_POTION: 'Max potion',
             Inventory.ITEM_RAZZ_BERRY: 'Razz berry',
             Inventory.ITEM_REVIVE: 'Revive',
             Inventory.ITEM_MAX_REVIVE: 'Max revive'}
TEAM_ID = {Enums.NEUTRAL: 'Neutral', Enums.BLUE: 'Blue', Enums.RED: 'Red', Enums.YELLOW: 'Yellow'}
POTIONS = [Inventory.ITEM_POTION, Inventory.ITEM_SUPER_POTION, Inventory.ITEM_HYPER_POTION, Inventory.ITEM_MAX_POTION]
REVIVES = [Inventory.ITEM_REVIVE, Inventory.ITEM_MAX_REVIVE]
BERRIES = [Inventory.ITEM_RAZZ_BERRY]
GYM_LURE_PRIOR = 2
RND_POKESTOP_LIM = 5
STEP_SCAN_POKEMON = 3
FREE_INVENTORY = 50

WAIT = 4
WAIT_GET = 8
WAIT_ERROR = 16
TRY_ERROR_COUNT = 10


# noinspection PyTypeChecker,PyUnresolvedReferences
class Worker:
    def __init__(self, config, pokemon_names):
        self.api = PGoApi(provider=config.get('auth_service'),
                          username=config.get(Un),
                          password=config.get('password'))

        # logs strings
        self.player_data = {}
        self.team = ''
        self.last_player_pokemons = ''
        self.last_player_info = ''
        self.last_nearby = ''

        # loop variables
        self._i = 0
        self.tries = 0

        # config variables
        self.config = config
        self.KEEP_SIMILAR = config.get('KEEP_SIMILAR', 0)
        self.EVOLVE = config.get('EVOLVE', False)
        self.CACHED = not config.get('cached', False)
        self.CATCH_PRIORITY = config.get('CATCH_PRIORITY', True)
        self.pos = None
        self.set_position(*config.get('location'))

        # static config arrays
        self.pokemon_names = pokemon_names
        self.levels = []
        self.cp_multipliers = []
        self.gym_levels = []
        self.pokemon_candy_map = {}
        self.upgrades = {}

        # available items
        self.level = 1
        self.dust = 0
        self.walked = 0
        self.top_iv = 100
        self.keep_cp = 0
        self.max_items = {}
        self.candies = {}
        self.berries = []
        self.potions = []
        self.revives = []
        self.pokeballs = []
        self.pokemon_for_gym = {}
        self.last_pokestops = []
        self.buddy_pokemon_id = None

        if config.get('debug'):
            self.api.set_logger(logging.getLogger(__name__))

    def set_position(self, lat, lng, alt):
        self.pos = (lat, lng, alt)
        self.api.set_position(lat, lng, alt)
        if self.player_data:
            wait(WAIT)
            self.api.player_update(latitude=f2i(lat), longitude=f2i(lng))

    def heartbeat(self):
        if self._i % 10 == 0:
            wait(WAIT)
            player_data = self.api.get_player().get(R, {}).get('GET_PLAYER', {}).get('player_data')
            if player_data:
                self.player_data = player_data
                self.team = player_data.get(T, '')
                self.dust = filter(lambda d: d[N] == 'STARDUST', player_data.get('currencies', []))[0].get('amount')
                self.buddy_pokemon_id = player_data.get('buddy_pokemon', {}).get('id')
                self.collect_daily_bonus(player_data.get('daily_bonus', {}).get('next_defender_bonus_collect_timestamp_ms'))

        if self._i == 0:
            wait(WAIT)
            res = self.api.download_item_templates().get(R, {}).get('DOWNLOAD_ITEM_TEMPLATES')
            if res:
                with open('accounts/settings.json', 'w') as f:
                    f.write(json.dumps(res, indent=2))
                for template in res['item_templates']:
                    if 'pokemon_settings' in template:
                        pokemon_settings = template['pokemon_settings']
                        self.pokemon_candy_map[pokemon_settings[Pid]] = {Ca: pokemon_settings.get('candy_to_evolve', 0), Fid: pokemon_settings[Fid]}
                    if 'player_level' in template:
                        player_level = template['player_level']
                        self.levels = player_level['required_experience']
                        self.cp_multipliers = player_level['cp_multiplier']
                    if 'gym_level' in template:
                        self.gym_levels = template['gym_level']['required_experience']
                    if 'pokemon_upgrades' in template:
                        self.upgrades = template['pokemon_upgrades']
                for pokemon_candy in self.pokemon_candy_map.values():
                    pokemon_candy['max_candy'] = max([x[Ca] for x in filter(lambda pc: pc[Fid] == pokemon_candy[Fid], self.pokemon_candy_map.values())])

        if self._i % 10 == 0:
            inventory_items = self.get_inventory_items()

            # use cache
            self.use_cache(inventory_items)

            # log player stats
            self.log_all(inventory_items)

            # update inner structures and variables
            self.set_items(inventory_items)
            self.pokemon_for_gym = get_pokemon_for_gym(inventory_items, 1, self.buddy_pokemon_id)

            # make some needed actions
            self.cleanup_items(inventory_items)
            self.cleanup_pokemons(inventory_items)
            self.heal_pokemons(inventory_items)
            self.hatch_egg(inventory_items)
            self.use_lucky_egg(inventory_items)
            self.use_incense(inventory_items)

        if self._i % STEP_SCAN_POKEMON == 0:
            while self.scan_around():
                pass

        # set team if needed
        self.set_team()

        self._i += 1

    def walk_to(self, loc):
        try:
            steps = get_route(self.pos, loc, self.config.get('GMAPS_API_KEY', ''))
        except NoResults:
            steps = get_route(self.pos, loc)

        for step in steps:
            for next_point in get_increments(self.pos, step, self.config.get('STEP_SIZE', 5)):
                self.set_position(*next_point)
                self.heartbeat()
                if vincenty(self.pos, loc).meters < 40:
                    return

    def go_to_fort(self):
        map_cells = self.get_map_cells()
        forts = flatmap('forts', map_cells)
        fort_pokemons = defaultdict(list)
        for pokemon in flatmap('nearby_pokemons', map_cells):
            fort_pokemons[pokemon.get('fort_id')].append(pokemon)

        if not forts:
            # output(1, 'Map error of getting pokestops')
            wait(WAIT_ERROR)
            self.tries += 1
            if self.tries < TRY_ERROR_COUNT:
                self.go_to_fort()
            else:
                output(2, 'Captcha!')
                exit(9)
                # return False
        self.tries = 0

        if self.CATCH_PRIORITY:
            forts_ids = []
            for fort_id in reversed(sorted(fort_pokemons, key=lambda fp: len(fort_pokemons[fp]))):
                if not len(forts_ids) or len(fort_pokemons[fort_id]) == forts_ids[0]:
                    forts_ids.append(fort_id)
            destinations = filter(lambda x: x[ID] in forts_ids, forts)
        else:
            destinations = filtered_forts(self.pos, forts, self.last_pokestops)
        # destinations = filter(lambda d: Afm in d, destinations) # for lure debug
        # destinations = filter(lambda d: 'type' not in d, destinations) # for gym debugging
        if destinations:
            fort = choice(destinations)
            wait(WAIT)
            fort_details = self.api.fort_details(fort_id=fort[ID]).get(R, {})['FORT_DETAILS']
            output(2, u"Walking to({4})[{5}] {0},{1} - {2}{3}".format(
                fort[Lt], fort[Lg], fort_details[N],
                (' (%s)' % fort_details['description']) if 'description' in fort_details else '',
                destinations.index(fort),
                (', '.join(map(lambda x: self.pokemon_names[str(x['pokemon_id'])], fort_pokemons.get(fort[ID])) if fort[ID] in fort_pokemons else ''))
            ))
            self.walk_to((fort[Lt], fort[Lg]))
            if fort.get('cooldown_complete_timestamp_ms', -1) < time() * 1000:
                self.spin_fort(fort, fort_details, len(forts))
        else:
            output(1, "No fort to walk to!")

    def spin_gym(self, fort, fort_details):
        if 'type' not in fort and fort.get(OBT) == self.team or fort.get(OBT) == Enums.NEUTRAL:
            wait(WAIT)
            res = self.api.get_gym_details(
                gym_id=fort[ID], gym_latitude=fort[Lt], gym_longitude=fort[Lg],
                player_latitude=self.pos[0], player_longitude=self.pos[1]
            ).get(R, {}).get('GET_GYM_DETAILS', {}).get('gym_state')

            gym_level = self.gym_levels.index(max(filter(lambda x: x <= res['fort_data'].get('gym_points', 0), self.gym_levels))) + 1
            if gym_level > len(res.get('memberships', 0)) and self.pokemon_for_gym.get(ID):
                wait(WAIT)
                res = self.api.fort_deploy_pokemon(fort_id=fort[ID],
                                                   pokemon_id=self.pokemon_for_gym.get(ID),
                                                   player_latitude=self.pos[0],
                                                   player_longitude=self.pos[1]).get(R, {}).get('FORT_DEPLOY_POKEMON', {})
                result = res.get(Re, -1)
                if result == 1:
                    gym_state = res.get('gym_state')
                    if fort_details and gym_state:
                        output(5, "%s deployed into '%s gym':  %s",
                               self.pokemon_format(self.pokemon_for_gym),
                               TEAM_ID[gym_state['fort_data'][OBT]],
                               fort_details[N])
                        self.pokemon_for_gym = {}
                    else:
                        output(1, "Deployed " + json.dumps(res))
                elif result == 2:
                    output(5, "Already pokemon in fort %s", fort_details[N])
                else:
                    output(1, "GymLevel %s %s", fort_details[N], json.dumps(res))

    def spin_fort(self, fort, fort_details=None, forts_len=None):
        if not fort_details:
            wait(WAIT)
            fort_details = self.api.fort_details(fort_id=fort[ID]).get(R, {})['FORT_DETAILS']

        if self.last_pokestops and fort[ID] == self.last_pokestops[0]:
            return

        self.spin_gym(fort, fort_details)

        wait(WAIT)
        fort_search = self.api.fort_search(
            fort_id=fort[ID], fort_latitude=fort[Lt], fort_longitude=fort[Lg],
            player_latitude=self.pos[0], player_longitude=self.pos[1])
        if not fort_search:
            return
        fort_search = fort_search.get(R, {}).get('FORT_SEARCH', {})

        result = fort_search.get(Re, -1)
        if result == 1:
            output(2, u"Visited: %s +%sEXP%s%s", fort_details[N],
                   fort_search[Ea] if Ea in fort_search else 0,
                   (" +%s items" % sum(i['item_count'] for i in fort_search['items_awarded']))
                   if 'items_awarded' in fort_search else '',
                   (" (%s gym)" % TEAM_ID.get(fort.get(OBT))) if fort.get(OBT) else '')
        elif result == 4:
            output(2, u"Visited: %s +%sEXP (Inventory full)%s", fort_details[N],
                   fort_search[Ea] if Ea in fort_search else 0,
                   (" (%s gym)" % TEAM_ID.get(fort.get(OBT))) if fort.get(OBT) else '')
        else:
            output(1, 'Visited ' + json.dumps(fort_search))

        # save visited pokestop
        self.last_pokestops = [fort[ID]] + self.last_pokestops
        if forts_len:
            self.last_pokestops = self.last_pokestops[:forts_len / 2]

        # catch lure pokemon
        if 'modifiers' in fort_details and self.pokeballs:
            map_cells = self.get_map_cells()
            forts = flatmap('forts', map_cells)
            forts = filter(lambda x: x[ID] == fort_details['fort_id'], forts)
            if forts and 'lure_info' in forts[0]:
                self.disk_encounter_pokemon(forts[0])
        return

    def log_all(self, items):
        # log player stats if needed
        currency_data = " ".join(map(lambda c: "{0}:{1}".format(c.get(N, '').title(), c.get('amount', 0)), self.player_data.get('currencies', [])))
        walked_data = "Walked:%s" % self.walked
        player_info = "%s%s %s %s" % (self.player_data.get(Un, ''), self.get_player_stats(items), currency_data, walked_data)
        if player_info != self.last_player_info and self.player_data:
            output(7, player_info)
            self.last_player_info = player_info

        # update pokemon stats
        player_pokemons = self.get_pokemon_stats(items)
        if player_pokemons != self.last_player_pokemons:
            output(7, player_pokemons)
            self.last_player_pokemons = player_pokemons

    def scan_around(self):
        map_cells = self.get_map_cells()
        catchable_pokemons = flatmap('catchable_pokemons', map_cells)
        nearby_pokemons = flatmap('nearby_pokemons', map_cells)
        wild_pokemons = flatmap('wild_pokemons', map_cells)

        # scan if there are any near pokestops
        for fort in filtered_forts_40(self.pos, flatmap('forts', map_cells)):
            self.spin_fort(fort)

        # for debug
        if len(catchable_pokemons) != len(wild_pokemons):
            output(1, "WildPokemons " + json.dumps([catchable_pokemons, wild_pokemons]))

        # log nearby pokemons
        if nearby_pokemons and catchable_pokemons:
            nearby_pokemons = sorted(nearby_pokemons, key=lambda x: x.get(DIM, 1000))
            encounters = map(lambda ps: ps['encounter_id'], catchable_pokemons)
            nearby = ', '.join([('*%s*[%d]' if p['encounter_id'] in encounters else '%s[%d]') % (self.pokemon_names[str(p[Pid])], int(p.get(DIM, 0))) for p in nearby_pokemons])
            nearby_encounters = map(lambda np: np['encounter_id'], nearby_pokemons)
            additional = ['*%s*' % self.pokemon_names[str(p[Pid])] for p in catchable_pokemons if p['encounter_id'] not in nearby_encounters]
            if additional:
                nearby += ' (%s)' % ', '.join(additional)
            if self.last_nearby != nearby:
                output(6, "Nearby: %s", nearby)
                self.last_nearby = nearby

        # catch random nearby pokemon
        return self.encounter_pokemon(choice(catchable_pokemons)) if catchable_pokemons and self.pokeballs else False

    def get_map_cells(self):
        lat = self.pos[0]
        lng = self.pos[1]
        cell_ids = get_cell_ids(lat, lng)
        timestamps = [0, ] * len(cell_ids)
        wait(WAIT_GET)
        res = self.api.get_map_objects(latitude=f2i(lat), longitude=f2i(lng), since_timestamp_ms=timestamps, cell_id=cell_ids)
        return res.get(R, {}).get('GET_MAP_OBJECTS', {}).get('map_cells', []) if res else []

    def get_inventory_items(self):
        wait(WAIT_GET)
        return map(lambda ii: ii.get('inventory_item_data', {}), self.api.get_inventory().get(R, {})['GET_INVENTORY']['inventory_delta']['inventory_items'])

    def cleanup_items(self, inventory_items):
        items = map(lambda i: i[I], filter(lambda i: I in i and C in i[I], inventory_items))
        items_count = sum(i[C] for i in items)
        if items_count > self.player_data.get('max_item_storage') - FREE_INVENTORY:
            recycled = []
            for item in items:
                max_item = self.max_items.get(item[Ii])
                if max_item and C in item and item[C] > max_item:
                    recycle_count = item[C] - (max_item if max_item > 0 else 0)
                    wait(WAIT)
                    resp = self.api.recycle_inventory_item(item_id=item[Ii], count=recycle_count)
                    if resp and resp.get(R, {}).get('RECYCLE_INVENTORY_ITEM', {}).get(Re, -1) == 1:
                        recycled.append('%s %s' % (recycle_count, ITEM_NAME[item[Ii]] if item[Ii] in ITEM_NAME else item[Ii]))
                        item[C] -= recycle_count
            if recycled:
                output(4, "Recycling: {0}".format(', '.join(recycled)))

    def cleanup_pokemons(self, inventory_items):
        all_pokemons = filter(lambda p: PD in p and 'cp' in p[PD] and 'favorite' not in p[PD] and 'deployed_fort_id' not in p[PD] and p[PD]['id'] != self.buddy_pokemon_id, inventory_items)
        caught_pokemon = defaultdict(list)
        for pokemon in all_pokemons:
            caught_pokemon[pokemon[PD][Pid]].append(pokemon[PD])

        # evolve pokemons
        pokemons_for_upgrade = []
        max_dust_cost = 0
        for pokemons in sorted(caught_pokemon.values(), key=lambda x: x[0][Pid], reverse=True):
            pokemon = max(pokemons, key=lambda x: iv(x))
            cp_total = self.cp_multipliers[self.level + 1]
            if self.pokemon_candy_map.get(pokemon[Pid]).get('candy', -1) == 0 and iv(pokemon) >= self.top_iv and cp(pokemon) < cp_total:
                pokemon['lvl'] = self.cp_multipliers.index(max(filter(lambda c: c <= cp(pokemon), self.cp_multipliers)))
                pokemon[Sc] = self.upgrades[Sc][pokemon['lvl']]
                pokemons_for_upgrade.append(pokemon)
                if pokemon[Sc] > max_dust_cost:
                    max_dust_cost = pokemon[Sc]
            if len(pokemons) > self.KEEP_SIMILAR:
                pokemon_candy = self.pokemon_candy_map[pokemon[Pid]]
                family = pokemon_candy[Fid]
                if self.EVOLVE and self.candies.get(family, 0) >= pokemon_candy['max_candy']:
                    wait(WAIT)
                    res = self.api.evolve_pokemon(pokemon_id=pokemon[ID])
                    if res:
                        res = res.get(R, {}).get('EVOLVE_POKEMON', {})
                        if res.get(Re, -1) == 1:
                            evolved_pokemon = res.get('evolved_pokemon_data')
                            output(3, "Evolve: %s > %s +%sEXP +%s candy, Used:%s/%s",
                                   self.pokemon_format(pokemon),
                                   self.pokemon_format(evolved_pokemon),
                                   res.get(Ea, 0),
                                   res.get('candy_awarded', 0),
                                   pokemon_candy[Ca],
                                   self.candies[family])
                            self.candies[family] -= pokemon_candy[Ca]
                            pokemons.remove(pokemon)
                            caught_pokemon[evolved_pokemon[Pid]].append(evolved_pokemon)

        # upgrade pokemons
        if self.EVOLVE and self.dust > max_dust_cost:
            for pokemon in pokemons_for_upgrade:
                pokemon_id = pokemon[Pid]
                pokemon_lvl = pokemon['lvl']
                candy_cost = self.upgrades['candy_cost'][pokemon_lvl]
                dust_cost = pokemon[Sc]

                family = self.pokemon_candy_map[pokemon_id][Fid]
                cp_total = self.cp_multipliers[self.level + 1]
                while self.candies.get(family, 0) >= candy_cost and self.dust > dust_cost and cp(pokemon) < cp_total and self.dust > max_dust_cost:
                    wait(WAIT_GET)
                    res = self.api.upgrade_pokemon(pokemon_id=pokemon[ID])
                    if res.get(R, {}).get('UPGRADE_POKEMON', {}).get(Re, -1) == 1:
                        output(3, "Upgrading: %s Used:%s/%s", self.pokemon_format(pokemon), candy_cost, self.candies[family])
                        self.candies[family] -= candy_cost
                        self.dust -= dust_cost
                        pokemon = res.get(R, {}).get('UPGRADE_POKEMON', {}).get('upgraded_pokemon')
                        pokemon_lvl += 1
                        candy_cost = self.upgrades['candy_cost'][pokemon_lvl]
                        dust_cost = self.upgrades[Sc][pokemon_lvl]
                    else:
                        output(1, "Upgrade " + json.dumps(res))

        # cleanup pokemons
        for pokemons in caught_pokemon.values():
            pokemon_iv = max(pokemons, key=lambda x: iv(x))
            pokemons_filtered = sorted(filter(lambda p: p[ID] != pokemon_iv[ID] and
                                                        iv(p) < self.top_iv and
                                                        p['cp'] < self.keep_cp, pokemons),
                                       key=lambda x: x['cp'],
                                       reverse=True)
            diff_len = len(pokemons) - 1 - len(pokemons_filtered)
            keep_from = 0 if diff_len > self.KEEP_SIMILAR else self.KEEP_SIMILAR - diff_len
            for pokemon in pokemons_filtered[keep_from:]:
                wait(WAIT)
                self.api.release_pokemon(pokemon_id=pokemon[ID])
                output(3, "Transfer(%s): %s", self.keep_cp, self.pokemon_format(pokemon))

    def attempt_catch(self, encounter_id, spawn_point_id, pokemon_data, lure=''):
        output(6, "%sA wild %s was appeared in %.1f meters",
               lure,
               self.pokemon_format(pokemon_data),
               distance(self.pos, pokemon_data))

        while self.pokeballs:
            try:
                current_pokeball = choice(self.pokeballs)
                nrs = gauss_down(2.0)
                sm = gauss_down(1.0)
                nhp = choice([1, 0])
                hit = True  # random() < (0.5 + 0.5 * self.level / len(self.levels))
                ball_name = ITEM_NAME[current_pokeball[Ii]]
                output(6, "Throw %s %.3f %.3f %s %s", ball_name, nrs, sm, 'in' if nhp else 'out', 'Hit' if hit else 'Miss')

                # use berry for cool throws
                if hit and nrs > 1.5:
                    berries = filter(lambda p: p[C] > 0, self.berries)
                    if berries:
                        berry = berries[0]
                        wait(WAIT_GET)
                        r = self.api.use_item_capture(item_id=Inventory.ITEM_RAZZ_BERRY, encounter_id=encounter_id, spawn_point_id=spawn_point_id)
                        berry[C] -= 1
                        if r.get(R, {}).get('USE_ITEM_CAPTURE', {}).get('success', False):
                            output(6, "%s used, %s left", ITEM_NAME[Inventory.ITEM_RAZZ_BERRY], berry[C])
                wait(WAIT_GET)
                r = self.api.catch_pokemon(
                    encounter_id=encounter_id,
                    pokeball=current_pokeball[Ii],
                    normalized_reticle_size=nrs,
                    spawn_point_id=spawn_point_id,
                    hit_pokemon=hit,
                    spin_modifier=sm,
                    normalized_hit_position=nhp,
                ).get(R, {})['CATCH_POKEMON']
                current_pokeball[C] -= 1
                capture_status = r.get('status')
                if capture_status == 0:
                    output(1, "Catch error: " + json.dumps(r))
                    # return r
                elif capture_status == 1:
                    output(6, "\tGotcha! %s +%sXP",
                           self.pokemon_format(pokemon_data),
                           sum(r['capture_award']['xp']))
                    return r
                elif capture_status == 2:
                    pass
                    # output(6, "%s escaped", self.pokemon_format(pokemon_data))
                elif capture_status == 3:
                    output(6, "%s run away", self.pokemon_format(pokemon_data))
                    return False
                elif capture_status == 4:
                    pass
                    # output(6, "%s missed", self.pokemon_format(pokemon_data))
                else:
                    output(1, "Fail attempt catch %s", json.dumps(r))
                    return False
                self.pokeballs = filter(lambda p: p.get(C, 0) > 0, self.pokeballs)
                if not self.pokeballs:
                    output(1, "No pokeballs")
            except ServerSideRequestThrottlingException as e:
                output(1, "Catch: %s", e.message)
                wait(WAIT_ERROR)

    def disk_encounter_pokemon(self, fort):
        try:
            encounter_id = fort['lure_info']['encounter_id']
            fort_id = fort[ID]
            wait(WAIT)
            encounter = self.api.disk_encounter(
                encounter_id=encounter_id,
                fort_id=fort[ID],
                player_latitude=self.pos[0],
                player_longitude=self.pos[1]
            ).get(R, {})['DISK_ENCOUNTER']

            if encounter[Re] == 1:
                encounter[PD].update({Lt: fort.get(Lt), Lg: fort.get(Lg)})
                self.attempt_catch(encounter_id, fort_id, encounter[PD], 'Lure: ')
        except Exception as e:
            output(1, "Error in disk encounter %s", e)
            return False

    def encounter_pokemon(self, pokemon):
        encounter_id = pokemon['encounter_id']
        spawn_point_id = pokemon['spawn_point_id']
        wait(WAIT)
        encounter = self.api.encounter(encounter_id=encounter_id,
                                       spawn_point_id=spawn_point_id,
                                       player_latitude=self.pos[0],
                                       player_longitude=self.pos[1]).get(R, {})['ENCOUNTER']
        if encounter['status'] == 1:
            pokemon.update(encounter.get('wild_pokemon', {}).get('pokemon_data'))
            return self.attempt_catch(encounter_id, spawn_point_id, pokemon)
        return False

    def heal_pokemons(self, inventory_items_dict_list):
        if self.potions and sum([x[C] for x in self.potions]) > 0:
            inventory_items_pokemon_list = filter(
                lambda p: PD in p and
                          'is_egg' not in p[PD] and
                          'deployed_fort_id' not in p[PD] and
                          p[PD].get(St, 0) < p[PD][Sm], inventory_items_dict_list)
            potions = filter(lambda p: p[C] > 0, self.potions)
            for pokemon in [x[PD] for x in inventory_items_pokemon_list]:
                while pokemon.get(St, 0) < pokemon.get(Sm) and potions:
                    if St in pokemon:
                        potions = filter(lambda p: p[C] > 0, self.potions)
                        potion = potions[0]
                        wait(WAIT)
                        resp = self.api.use_item_potion(item_id=potion[Ii], pokemon_id=pokemon[ID])
                        result = resp.get(R, {}).get('USE_ITEM_POTION', {}).get(Re, -1)
                        if result == 1:
                            pokemon[St] = resp.get(R, {}).get('USE_ITEM_POTION', {}).get(St, 0)
                            output(5, "Healed %s with %s to %sHP",
                                   self.pokemon_format(pokemon),
                                   ITEM_NAME[potion[Ii]],
                                   pokemon[St])
                            potion[C] -= 1
                        else:
                            output(1, "HealPokemons " + json.dumps(resp))
                            break
                    else:
                        revives = filter(lambda p: p[C] > 0, self.revives)
                        if revives:
                            revive = revives[0]
                            wait(WAIT)
                            resp = self.api.use_item_revive(item_id=revive[Ii], pokemon_id=pokemon[ID])
                            result = resp.get(R, {}).get('USE_ITEM_REVIVE', {}).get(Re, -1)
                            if result == 1:
                                pokemon[St] = resp.get(R, {}).get('USE_ITEM_REVIVE', {}).get(St, 0)
                                output(5, "Healed %s with %s to %sHP",
                                       self.pokemon_format(pokemon),
                                       ITEM_NAME[revive[Ii]],
                                       pokemon[St])
                                revive[C] -= 1
                            else:
                                output(1, "HealPokemonsRevive " + json.dumps(resp))
                                break
                        else:
                            break

    def hatch_egg(self, items):
        wait(WAIT)
        hatched_eggs = self.api.get_hatched_eggs().get(R, {})['GET_HATCHED_EGGS']
        if hatched_eggs and Pid in hatched_eggs:
            pokemon_ids = hatched_eggs[Pid]

            inventory_items = self.get_inventory_items()
            inventory_items = [self.pokemon_format(x[PD]) for x in inventory_items if PD in x and x[PD][ID] in pokemon_ids]

            output(5, "Oh! %s +%sEXP, Stardust:+%s Candy:+%s ",
                   ', '.join(inventory_items),
                   sum(hatched_eggs[Ea]),
                   sum(hatched_eggs['stardust_awarded']),
                   sum(hatched_eggs['candy_awarded']))

        incubator_ids = map(lambda x: x[ID], filter(lambda i: Pid not in i, filter(lambda i: 'egg_incubators' in i, items)[0].get('egg_incubators').get('egg_incubator')))

        if incubator_ids:
            incubator_id = incubator_ids[0]
            try:
                eggs = [x[PD] for x in items if
                        x.get(PD, {}).get('is_egg', False) and
                        'egg_incubator_id' not in x.get(PD, {})]
                if eggs:
                    egg = max(eggs, key=lambda e: e['egg_km_walked_target'])
                    wait(WAIT)
                    resp = self.api.USE_ITEM_EGG_INCUBATOR(item_id=incubator_id, pokemon_id=egg[ID])
                    code = resp.get(R, {}).get('USE_ITEM_EGG_INCUBATOR', {}).get(Re, 0)
                    if code == 1:
                        output(5, 'Start hatch egg %skm', egg['egg_km_walked_target'])
            except StopIteration:
                pass

                # walked_eggs = []
                # for incubator in filter(lambda i: Pid in i, filter(lambda i: 'egg_incubators' in i, items)[0].get('egg_incubators').get('egg_incubator')):
                #     walked_eggs.append("%.2f/%.1f" % (889.0281372070312-incubator.get('start_km_walked'),incubator.get('target_km_walked') - incubator.get('start_km_walked')))
                # output(2, "Eggs progress: %s", ', '.join(walked_eggs))

    def use_lucky_egg(self, items):
        applied_items = filter(lambda x: 'applied_items' in x, items)
        if applied_items:
            applied_items = applied_items[0]['applied_items'][I]
            applied_eggs = filter(lambda x: x.get(Ii) == Inventory.ITEM_LUCKY_EGG and x['expire_ms'] / 1000 > time(), applied_items)
            if applied_eggs:
                return

        eggs = filter(lambda x: x.get(I, {}).get(Ii) == Inventory.ITEM_LUCKY_EGG, items)
        if eggs and eggs[0][I].get(C, 0) > 0:
            wait(WAIT)
            resp = self.api.use_item_xp_boost(item_id=Inventory.ITEM_LUCKY_EGG).get(R, {}).get('USE_ITEM_XP_BOOST', {}).get(Re, 0)
            if resp == 1:
                output(5, 'Lucky egg used, %s left', eggs[0][I].get(C, 0) - 1)

    def use_incense(self, items):
        applied_items = filter(lambda x: 'applied_items' in x, items)
        if applied_items and self.pokeballs:
            applied_items = applied_items[0]['applied_items'][I]
            applied_incense = filter(lambda x: x.get(Ii) == Inventory.ITEM_INCENSE_ORDINARY and x['expire_ms'] / 1000 > time(), applied_items)
            if applied_incense:
                wait(WAIT)
                r = self.api.get_incense_pokemon(player_latitude=self.pos[0], player_longitude=self.pos[1]).get(R, {}).get('GET_INCENSE_POKEMON', {})
                result = r.get(Re, -1)
                if result == 1:  # pokemon available
                    encounter_location = r.get('encounter_location')
                    encounter_id = r.get('encounter_id')
                    wait(WAIT_GET)
                    encounter = self.api.incense_encounter(encounter_id=encounter_id, encounter_location=encounter_location).get(R, {}).get('INCENSE_ENCOUNTER', {})
                    if encounter.get(Re, -1) == 1:
                        pokemon_data = encounter.get('pokemon_data')
                        pokemon_data.update({Lt: r.get('latitude'), Lg: r.get('longitude')})
                        self.attempt_catch(encounter_id, encounter_location, pokemon_data, "Incense: ")
                return

        incenses = filter(lambda x: x.get(I, {}).get(Ii) == Inventory.ITEM_INCENSE_ORDINARY, items)
        if incenses and incenses[0][I].get(C, 0) > 0:
            wait(WAIT)
            r = self.api.use_incense(incense_type=Inventory.ITEM_INCENSE_ORDINARY).get(R, {}).get('USE_INCENSE', {})
            if r.get(Re, -1) == 1:
                output(5, 'Incense used, %s left', incenses[0][I].get(C, 0) - 1)

    def use_cache(self, inventory_items):
        if self.CACHED and os.path.isfile("accounts/%s.json" % self.config[Un]):
            with open("accounts/%s.json" % self.config[Un], "r") as f:
                cache = f.read()
                if cache:
                    cache = json.loads(cache)
                    if l in cache:
                        self.level = cache.get(l)
                    if T in cache:
                        self.team = cache.get(T)
                    if Lt in cache and Lg in cache:
                        output(2, 'Set position from cache %s,%s' % (cache[Lt], cache[Lg]))
                        self.set_position(cache[Lt], cache[Lg], 0)
                    if 'last_pokestops' in cache:
                        self.last_pokestops = cache.get('last_pokestops')
                self.CACHED = False
        else:
            with open("accounts/%s.json" % self.config[Un], "w") as f:
                res = {Lt: self.pos[0], Lg: self.pos[1], 'inventory_items': inventory_items}
                output(2, "Now: %.6f,%.6f", self.pos[0], self.pos[1])
                if self.player_data:
                    res['player_data'] = self.player_data
                if self.level > 1:
                    res[l] = self.level
                if self.team:
                    res[T] = self.team
                if self.last_pokestops:
                    res['last_pokestops'] = self.last_pokestops
                f.write(json.dumps(res, indent=2))

    def set_team(self):
        if self.team == '' and self.level >= 5:
            wait(WAIT)
            res = self.api.set_player_team(team=Enums.BLUE).get(R, {}).get('SET_PLAYER_TEAM', {})
            if res.get('status', -1) == 1:
                output(7, 'Team successfully set')
                self.team = Enums.BLUE
            else:
                output(7, 'Team set ' + json.dumps(res))

    def set_items(self, items):
        self.max_items = {}
        for item in ITEM_COUNTS:
            self.max_items[item] = ITEM_COUNTS[item](self.level)
        self.candies = {}
        for candy in [c[Ca] for c in filter(lambda x: Ca in x and Ca in x[Ca], items)]:
            self.candies[candy[Fid]] = candy[Ca]
        self.potions = items_by_type(items, POTIONS)
        self.revives = items_by_type(items, REVIVES)
        self.berries = items_by_type(items, BERRIES)
        self.pokeballs = filter(lambda p: p.get(C, 0) > 0, map(lambda x: x[I], filter(lambda x: x.get(I, {}).get(Ii, 0) in xrange(1, 4), items)))

    def collect_daily_bonus(self, timestamp):
        if timestamp and timestamp < time() * 1000:
            res = self.api.collect_daily_defender_bonus().get(R, {}).get('COLLECT_DAILY_DEFENDER_BONUS', {})
            if res.get(Re, -1) == 1:
                currency_type = res.get(R, {}).get('COLLECT_DAILY_DEFENDER_BONUS', {}).get('currency_type', [])
                currency_awarded = res.get(R, {}).get('COLLECT_DAILY_DEFENDER_BONUS', {}).get('currency_awarded', [])
                defenders_count = res.get(R, {}).get('COLLECT_DAILY_DEFENDER_BONUS', {}).get('defenders_count', 0)
                awarded_string = []
                for i in range(len(currency_type)):
                    awarded_string.append("%s:+%s" % (currency_type[i], currency_awarded[i]))
                output(5, 'Defender bonus(%s): %s', defenders_count, ', '.join(awarded_string))
            else:
                output(2, 'Defender bonus ' + json.dumps(res))

    def get_pokemon_stats(self, items):
        items = map(lambda x: x[PD], filter(lambda x: PD in x and 'is_egg' not in x[PD], items))
        items = sorted(items, key=lambda x: iv(x), reverse=True)
        self.top_iv = (sum(iv(i) for i in items) / (len(items)) + 90) / 3.0 if self.config.get('MIN_KEEP_IV', 0) == 0 else self.config.get('MIN_KEEP_IV', 0)
        keep_cp_over = self.config.get('KEEP_CP_OVER', 1)
        self.keep_cp = keep_cp_over if keep_cp_over > 1 else sum(p['cp'] for p in items) / len(items) * 2 * keep_cp_over
        return ' '.join(map(lambda x: self.pokemon_format(x), items))

    def get_player_stats(self, items):
        inventory_stats = filter(lambda x: 'player_stats' in x, items)
        if inventory_stats:
            player_stats = inventory_stats[0].get('player_stats', {})
            self.walked = player_stats.get('km_walked', 0)
            level = player_stats.get(l, 1)
            if self.level != level:
                self.level = level
                wait(WAIT)
                res = self.api.level_up_rewards(level=level).get(R, {}).get('LEVEL_UP_REWARDS', {})
                result = res.get(Re, -1)
                if result == 1:
                    output(7, 'Level up rewards received')
                elif result == 2:
                    output(7, 'Level up rewards already received')
                else:
                    output(2, 'Level up ' + json.dumps(res))
            return "(%s): %s/%sXP" % (level, player_stats.get('experience', 0) - self.levels[level - 1], self.levels[level] - self.levels[level - 1])
        return ''

    def pokemon_format(self, pd):
        return "%s[%s %s]" % (self.pokemon_names[str(pd[Pid])], pd['cp'], iv(pd))

    def main_loop(self):
        try:
            self.heartbeat()
        except ServerSideRequestThrottlingException:
            wait(WAIT_ERROR)
            self.main_loop()
        destination = self.config.get('walk_to')
        if destination:
            output(2, u"Walking {0},{1} > {2},{3}".format(self.pos[0], self.pos[1], destination[0], destination[1]))
            self.walk_to(destination)
        while True:
            try:
                self.heartbeat()
                self.go_to_fort()
            except (ServerSideRequestThrottlingException, KeyError):
                print_exc()
                wait(WAIT_GET)
            except TypeError:
                print_exc()
                self.__init__(self.config, self.pokemon_names)
            except NotLoggedInException:
                self.__init__(self.config, self.pokemon_names)
            except (SysCallError, AttributeError):
                wait(WAIT_GET)


def flatmap(key, items):
    return list(chain.from_iterable(imap(lambda i: i.get(key, []), items)))


def get_route(start, end, api_key=None):
    origin = (start[0], start[1])
    destination = (end[0], end[1])
    try:
        directions_service = Directions(api_key=api_key) if api_key else Directions()
        d = directions_service.directions(origin, destination, mode="walking", units="metric")
        return [(step['end_location']['lat'], step['end_location']['lng']) for step in d[0]['legs'][0]['steps']] + [destination]
    except GmapException:
        return [destination]


# step_size is how many meters we want; change that in config.json
def get_increments(start, end, step=10):
    g = pyproj.Geod(ellps='WGS84')
    (start_lat, start_long, _) = start
    (end_lat, end_long) = end
    (az12, az21, dist) = g.inv(start_long, start_lat, end_long, end_lat)
    # line paths that are <= 1 kilometer
    lon_lats = g.npts(start_long, start_lat, end_long, end_lat, 1 + int(dist / step))
    lon_lats.append((end_long, end_lat))
    return [(l[1], l[0], 0) for l in lon_lats]


def distance(p1, obj):
    return vincenty(p1, (obj[Lt], obj[Lg])).meters


def filtered_forts(pos, forts, last_forts):
    forts = filter(lambda fort: fort[ID] not in last_forts and fort.get('cooldown_complete_timestamp_ms', -1) < time() * 1000, forts)
    return sorted(forts, key=lambda f: distance(pos, f) * (1 if 'type' not in f or 'active_fort_modifier' in f else GYM_LURE_PRIOR))[:RND_POKESTOP_LIM]


def filtered_forts_40(pos, forts):
    return [f for f in forts if ('enabled' in f or 'lure_info' in f) and distance(pos, f) < 40 and f.get('cooldown_complete_timestamp_ms', -1) < time() * 1000]


def cp(pd):
    return pd.get('cp_multiplier', 0) + pd.get('additional_cp_multiplier', 0)


def iv(pd):
    return sum(pd.get(t, 0) for t in ['individual_attack', 'individual_stamina', 'individual_defense'])


def items_by_type(items, types):
    return sorted([p.get(I) for p in filter(lambda x: I in x and C in x[I] and x[I][Ii] in types, items)], key=lambda x: x[Ii])


def get_pokemon_for_gym(inventory_items, index, buddy_pokemon_id):
    inventory_items = filter(lambda x:
                             PD in x and
                             'is_egg' not in x[PD] and
                             'favorite' not in x[PD] and
                             'deployed_fort_id' not in x[PD] and
                             x[PD]['id'] != buddy_pokemon_id and
                             x[PD].get(St, 0) == x[PD][Sm], inventory_items)
    inventory_items = sorted(inventory_items, key=lambda x: x[PD]['cp'], reverse=True)
    return inventory_items[index][PD] if len(inventory_items) > index else None


def wait(t):
    sleep(gauss(t, 0.2 * t))


def gauss_down(v):
    return v - abs(gauss(0, 0.3 * v))


# 0 white\black
# 1 red
# 2 green
# 3 yellow
# 4 blue
# 5 pink
# 6 cyan
# 7 gray
def output(color, message, *args):
    print "[%s]%s" % (strftime("%Y-%m-%d %H:%M:%S"), '\x1b[1;%s;40m%s\x1b[0m' % (str(30 + color), message % args))
